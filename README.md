# parallel_nth_element

## Descrizione 
Implementazione parallela funzione nth\_element, ordinamneto parziale di un vettore, presente nella libreria del C++11.
Questa soluzione � stata implementata con i Pthread al fine di parallelizzare l'ordinamento. 

L'ordinamento avviene su vettore di 10e7 elementi con 4 thread

### include contiene:

- p_nth_element.h
- mergesort.h -> funzione di ordinamento usata dalla p_nth_element per ordinare i samples
- settings.h -> parametri del p_nth_element
- singlelock-ms.h -> coda shared

## Teconologie
1. g++ 6 
2. pthread (libreria nativa)
3. fopen

## Compilazione ed esecuzione 

Sostiure nel Makefile il g++-mp-6 con link al compilatore g+ version 6  

```sh
$ make
$ ./demo_p_nth_element
```

## Risultati:

I risultati delle computazioni sono memorizzate in un file di testo
