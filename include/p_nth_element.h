//function template p_nth_element
#ifndef p_nth_element_hpp
#define p_nth_element_hpp

#include <math.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <typeinfo>
#include <iostream>
#include <array>
#include <vector>
#include <iterator>
#include <algorithm>
#include <random>
#include <queue>
#include <ctime>
#include <utmpx.h>
#include <map>
#include <iomanip>
#include "mergesort.h"  // sorting degli splitter
#include "singlelock-ms.h" // shared queue
#include "settings.h" // settings p_nth_element

// DATA STRUCTURES
// struct of p_swap_chunk
template <typename _iterator_random, class _t_comparator,typename _ValueType>
struct _swap_chunk_parallel{
    int id;
    _iterator_random s_right, e_right , s_left, e_left; // 4 iterator two for left and two for right block
    _t_comparator *c; // comparator
    _ValueType single_splitter; // splitter
    _iterator_random ret_left_not_ordered,ret_right_not_ordered ; // 2 iterator, are used to point at the first element greater than splitter about left and right thread
};

// struct of p_swap_blocks
template <typename _iterator_random , class _t_comparator >
struct _swap_blocks_parallel{
    int id;
    queue_spmc_single<std::array<_iterator_random, 4>> *pq; // shared queue with pair of blocks to swap
};

// FUNCTIONS DEFINITION
template<class _iterator_random, class _t_comparator , typename _ValueType> void *p_swap_chunk(void *);
template<class _iterator_random, class _t_comparator> void *p_swap_blocks(void *); 
template <class _random , class _compare > void p_nth_element(_random, _random, _random, _compare*);

// FUNCTIONS IMPLEMENTATION
template <class _random , class _compare>
void p_nth_element(_random begin, _random nth , _random end , _compare* c){
    
    typedef std::iterator_traits<_random> _TraitsType;
    typedef typename _TraitsType::value_type _ValueType;

    // Singleton Class use to get and set set parameter of simulation
    Parameters* _s = Parameters::getInstance();
    
    int num_of_threads = _s->get_threads(); // number of threads used
    int block_size = _s->get_block_size();
    int minimal_size = _s->get_minimal_size_vector();
    int size_seq_swap = _s->get_min_size_sequential_swap();
    
    int *num_buckets;
    num_buckets = (int *)malloc(sizeof(int));
    int *NUM_SAMPLES;
    NUM_SAMPLES = (int *)malloc(sizeof(int));
    int *NUM_SPLITTERS;
    NUM_SPLITTERS = (int *)malloc(sizeof(int));
    
    // Struct of threads
    struct _swap_chunk_parallel<_random, _compare,_ValueType > *resp;
    struct _swap_chunk_parallel<_random, _compare, _ValueType> *_swap_chunks_thr;
    struct _swap_blocks_parallel<_random, _compare> *swap_blocks_thr;
    
    int size =  (int)std::distance(begin, end);
    int position = (int)std::distance(begin, nth);
    
    //generator inizialization
    int seed = (int)time(NULL);
    srand(seed);

    
    while(size > minimal_size && num_of_threads > 0 ){
        
       if (num_of_threads > 3){   
           if(size < 50000)
                num_of_threads = (int)num_of_threads/2;
            else if(size < 1000000)
                num_of_threads = (int)num_of_threads/2;
            else if(size < 5000000)
                num_of_threads = (int)num_of_threads/2;
        }
        
        pthread_t *ptr_thread;
        ptr_thread = (pthread_t*)malloc(num_of_threads*sizeof(pthread_t));

        *num_buckets = _s->get_num_buckets();
        *NUM_SPLITTERS = *num_buckets - 1;
        *NUM_SAMPLES = 10 * (*num_buckets);
        
        //threads allocation
        
        std::vector<_random> samples = {}; //vector of samples for splitter
        std::vector<_ValueType> vector_of_splitter = {}; //vector of  splitter (copy of elements)
        std::vector<int> count_elements_each_bucket = {}; //number of elements in each bucket

        for(int i = 0; i < *NUM_SAMPLES; i++){
            /* get random position  */
            int rand_val = rand() % size + 1;
            _random n = std::next(begin, rand_val); 
            samples.push_back(n);
        }
        
        // sort vector of samples
        KW::merge_sort(samples.begin(),samples.end(), c);
        
        // select splitters from samples
        for(int i = 0 ; i < (*num_buckets - 1) ; i++){
            auto n = (*samples[(i * (*num_buckets) + 5 ) % (10 * (*num_buckets) -1)]);
            vector_of_splitter.push_back(n);
            count_elements_each_bucket.push_back(0);
        }
        count_elements_each_bucket.push_back(0);
        
        //save begin used on next iteration
        _random current_begin = begin;
        
        /*
         * use each splitter to generate s-1 buckets
         */
        
        for( int k = 0; k < (int)vector_of_splitter.size(); k++){
            
            //init param
            int current_size = (int)std::distance(begin, end); // vector size
            int n_elem = current_size / (num_of_threads*2); // size of blocks
            int last_elements = current_size %(num_of_threads*2); // rest of division
            
            /*At the first step each thread get 2 blocks of vector, the vector will be split in threads * 2 blocks.
            Example of division with 4 threads: the vector will be split in 8 blocks and threads gets blocks as follows

            thread[0]: block[0]-block[4]
            thread[1]: block[1]-block[5]
            thread[2]: block[2]-block[6]
            thread[3]: block[3]-block[7]
            */
            
            //each thread return a tuple composte three elements
            // left   : is the first element minor of splitter v_splitter[k]
            // median : is the first element major of splitter v_splitter[k]
            // right  : is the last  element major of splitter v_splitter[k]
            std::map<int,std::vector<_random>> delimitator_of_sequence = {};
            
            // allocate a struct for sorting subsection of each thread
            _swap_chunks_thr = (_swap_chunk_parallel<_random, _compare ,  _ValueType>*)malloc(num_of_threads*sizeof(_swap_chunk_parallel<_random, _compare ,  _ValueType >));
            
            //fork  and join
            for(int t = 0; t <num_of_threads;  t++){
                
                // data of thread            
                _swap_chunks_thr[t].id = t;
                _swap_chunks_thr[t].c = c;
                _swap_chunks_thr[t].single_splitter = vector_of_splitter[k];
                _swap_chunks_thr[t].s_left = std::next(begin, t * n_elem);
                _swap_chunks_thr[t].e_left = std::next(begin, (t+1) * n_elem);
                _swap_chunks_thr[t].s_right = std::next(begin, (t+ num_of_threads) * n_elem);
                
                //define the last thread chunk size as n_element + rest
                if(t + 1 == num_of_threads)
                    _swap_chunks_thr[t].e_right = std::next(begin, (t+ num_of_threads +1)*n_elem + last_elements);
                else
                    _swap_chunks_thr[t].e_right = std::next(begin, (t+ num_of_threads +1) * n_elem);
                int rc = pthread_create(&ptr_thread[t], NULL, p_swap_chunk<_random , _compare ,_ValueType>, (void *)&_swap_chunks_thr[t]);
                
                if(rc){
                    printf("ERROR; return code from pthread_create() is %d\n", rc);
                    exit(-1);
                }
            }
            
            //join of thread
            for(int t = 0; t < num_of_threads ; t++) {
                
                int rc = pthread_join(ptr_thread[t], (void**)&resp);
                
                // resp pointer to structur retuned by each thread
                delimitator_of_sequence[t].push_back(resp->s_left); // first element of subsection minor than splitter
                delimitator_of_sequence[t].push_back(resp->ret_left_not_ordered);// first element of subsection major than splitter
                delimitator_of_sequence[t].push_back(resp->e_left); // last element of subsection major than splitter
                
                // resp pointer of struct retuned by each thread
                delimitator_of_sequence[t+num_of_threads].push_back(resp->s_right); // first element of subsection minor than splitter
                delimitator_of_sequence[t+num_of_threads].push_back(resp->ret_right_not_ordered);// first element of subsection major than splitter
                delimitator_of_sequence[t+num_of_threads].push_back(resp->e_right); // last
                if (rc) {
                    printf("ERROR; return code from pthread_join() is %d\n", rc);
                    exit(-1);
                }
            }
            
            //consider the special case of 1 thread
            if(num_of_threads > 1){
                
                // 4 iterators used to define two blocks, left and right block,
                // first and second are used to define first sequence to be swap with second sequence defined by third and fourth iterator
                std::array<_random, 4> two_blocks_swapping = {};
                
                // queue pair of blocks used by main thread to define all two_blocks_swapping
                std::queue<std::array<_random, 4>> *blocks_to_swap= new std::queue<std::array<_random, 4>>;
                
                //shared queue: each element is two_blocks_swapping and is got by threads
                queue_spmc_single<std::array<_random, 4>> *queue_sequence_swapping = new queue_spmc_single<std::array<_random, 4>>();
                
                // first element major than the splitter vector_of_splitter[k] used by main thread to count number of elements minor and major
                _random first_element_major_splitter = begin;
                
                for (int i = 0; i < 2 * num_of_threads ; i++) {
                    // fist element major than spitter
                    first_element_major_splitter += int(std::distance(delimitator_of_sequence[i][0], delimitator_of_sequence[i][1]));
                    
                    // total numer of element for each bucket
                    count_elements_each_bucket[k] += int(std::distance(delimitator_of_sequence[i][0], delimitator_of_sequence[i][1]));
                }
                
                // first thread
                int thread_left = 0;
                // last thread
                int thread_right = 2 * num_of_threads - 1;
                
                while(true){
                    
                    //dist_not_ordered_thr_left indicate number of elements major than splitter about thread left
                    int dist_not_ordered_thr_left = int(std::distance(delimitator_of_sequence[thread_left][1],delimitator_of_sequence[thread_left][2]));
                    //dist_ordered_thr_right indicate number of elements a minor than splitter about thread right
                    int dist_ordered_thr_right = int(std::distance(delimitator_of_sequence[thread_right][0], delimitator_of_sequence[thread_right][1]));
                    
                    // condition of termination
                    if(thread_left >= thread_right)
                        break;
                    
                    // no elements to swap
                    if((dist_not_ordered_thr_left == 0) && (dist_ordered_thr_right == 0)){
                        thread_left++;
                        thread_right--;
                        continue;
                    }
                    
                    if((dist_not_ordered_thr_left > 0) && (dist_ordered_thr_right == 0)){
                        thread_right--;
                        continue;
                    }
                    
                    if((dist_not_ordered_thr_left == 0) && (dist_ordered_thr_right > 0)){
                        thread_left++;
                        continue;
                    }
                    
                    // if thread_left has more minor elements and thread_right has less major elements 
                    // this condition swap all ordered elements from thread right and leave some elements of thread_left because the not_oredered are major than ordered
                    if(dist_not_ordered_thr_left > dist_ordered_thr_right){
                        
                        // if number of elements are minor than size_seq_swap, this swap is make by main thread
                        if(dist_ordered_thr_right <= size_seq_swap){
                            
                            _random a = delimitator_of_sequence[thread_left][1];
                            
                            // swap each element from thread_right with elements from thread_left and leave some elements on thread_left not moved, and mark it not swapped
                            for(_random b = delimitator_of_sequence[thread_right][1]-1; b >= delimitator_of_sequence[thread_right][0]; b-- ){
                                std::iter_swap(a, b);
                                a++;
                            }
                            
                            //update iterator position, move thread left iterator forward and the iterator of thread right is moved backward
                            delimitator_of_sequence[thread_left][1]= delimitator_of_sequence[thread_left][1]+ dist_ordered_thr_right;
                            
                            thread_right--; //remove this thread
                            continue;
                        }
                        
                        //push a quadruple like two_blocks_swapping
                        blocks_to_swap->push({{delimitator_of_sequence[thread_left][1],delimitator_of_sequence[thread_left][1]+dist_ordered_thr_right,delimitator_of_sequence[thread_right][0],delimitator_of_sequence[thread_right][1]}});
                        // update position like before
                        delimitator_of_sequence[thread_left][1]= delimitator_of_sequence[thread_left][1]+ dist_ordered_thr_right;
                        thread_right--;
                        
                    // opposite of dist_not_ordered_thr_left > dist_ordered_thr_right , in this, all elements (thread_left) minor are swapper with
                    // some elements (thread_right) major
                    }else if(dist_not_ordered_thr_left < dist_ordered_thr_right ){
                        
                        if(dist_not_ordered_thr_left <= size_seq_swap){
                            
                            _random a = delimitator_of_sequence[thread_left][1];
                            
                            for(_random b = delimitator_of_sequence[thread_right][1]-1; b >= delimitator_of_sequence[thread_right][1] - dist_not_ordered_thr_left; b-- ){
                                std::iter_swap(a, b);
                                a++;
                            }
                            
                            delimitator_of_sequence[thread_right][1]= delimitator_of_sequence[thread_right][1] - dist_not_ordered_thr_left;
                            thread_left++;
                            continue;
                        }
                        
                        blocks_to_swap->push({{delimitator_of_sequence[thread_left][1],delimitator_of_sequence[thread_left][2],delimitator_of_sequence[thread_right][1] - dist_not_ordered_thr_left ,delimitator_of_sequence[thread_right][1] }});
                        
                        delimitator_of_sequence[thread_right][1] = delimitator_of_sequence[thread_right][1] - dist_not_ordered_thr_left;
                        thread_left++;
                        
                    //if thread_left and thread_right has the same number of elements to swap
                    }else if(dist_ordered_thr_right == dist_not_ordered_thr_left){
                        
                        if(dist_ordered_thr_right <= size_seq_swap){
                            _random a = delimitator_of_sequence[thread_left][1];
                            
                            for(_random b = delimitator_of_sequence[thread_right][1]-1; b >=  delimitator_of_sequence[thread_right][1] - dist_ordered_thr_right; b-- ){
                                std::iter_swap(a, b);
                                a++;
                                
                            }
                            
                            thread_left++;
                            thread_right--;
                            continue;
                        }
                        
                        blocks_to_swap->push({{delimitator_of_sequence[thread_left][1],delimitator_of_sequence[thread_left][2],delimitator_of_sequence[thread_right][0],delimitator_of_sequence[thread_right][1]}});
                        
                        thread_left++;
                        thread_right--;
                        
                    }else{
                        printf("OUT OF CONDITION \n");
                        exit(0);
                    }
                }
                //build a shared queue from block saved in blocks_to_swap generated by main thread, this resize each blocks size if number of items is greater than size_of_block
                while (blocks_to_swap->size() > 0){
                    
                    std::array<_random, 4> sequence_swapping = blocks_to_swap->front();
                    blocks_to_swap->pop();
                    
                    //condition without size block definition
                    if (block_size == -1 ){
                        queue_sequence_swapping->push(sequence_swapping);                        
                    }else{
                        // create a quadruple of elements, each thread move a number of elements defined by size_block
                        // push a new element with block_size in a shared queue, and each thread pop from this queue an object made of two block and swap each element from block one to block two
                        for(int j=0; j < floor((int)std::distance(sequence_swapping[0], sequence_swapping[1])/block_size); j++ ){
                            two_blocks_swapping = {{sequence_swapping[0] + j * block_size, sequence_swapping[0] + (j+1) * block_size , sequence_swapping[2] + j * block_size, sequence_swapping[2] + (j+1) * block_size}};
                            queue_sequence_swapping->push(two_blocks_swapping);
                        }
                        // condition when there is a rest of division
                        if((int)std::distance(sequence_swapping[0], sequence_swapping[1]) % block_size != 0){
                            two_blocks_swapping = {{sequence_swapping[0]+ floor((int)std::distance(sequence_swapping[0], sequence_swapping[1])/block_size) * block_size, sequence_swapping[1] , sequence_swapping[2]+ floor((int)std::distance(sequence_swapping[0], sequence_swapping[1])/block_size) * block_size, sequence_swapping[3] }};
                            queue_sequence_swapping->push(two_blocks_swapping);
                        }
                    }
                }
                
                swap_blocks_thr = (_swap_blocks_parallel<_random, _compare>*)malloc(num_of_threads*sizeof(_swap_blocks_parallel<_random, _compare>));
                
                for (int t = 0; t < num_of_threads; t++) {
                    
                    swap_blocks_thr[t].id = t;
                    swap_blocks_thr[t].pq = queue_sequence_swapping;
                    int rc = pthread_create(&ptr_thread[t], NULL, p_swap_blocks<_random, _compare>, (void *) &swap_blocks_thr[t]);
                    
                    if (rc) {
                        printf("ERROR; return code from pthread_create() is %d\n", rc);
                        exit(-1);
                    }
                }
                
                for (int t = 0; t < num_of_threads; t++) {                    
                    int rc = pthread_join(ptr_thread[t], NULL);
                    
                    if (rc) {
                        printf("ERROR; return code from pthread_join() is %d\n", rc);
                        exit(-1);
                    }
                }
                
                begin = first_element_major_splitter ;
                
            }else{ // if is there only one thread
                int ord_thread_left = int(std::distance(delimitator_of_sequence[0][0],delimitator_of_sequence[0][1]));
                int ord_thread_right = int(std::distance(delimitator_of_sequence[1][0],delimitator_of_sequence[1][1]));
                int dist_element_ord = ord_thread_left+ ord_thread_right;
                count_elements_each_bucket[k] = dist_element_ord;
                begin = std::next(begin, dist_element_ord);
            }
            
            //clean heap memory
            delete _swap_chunks_thr;
        }
        
        //update the number of elements in the last bucket
        
        count_elements_each_bucket[vector_of_splitter.size()] = int(std::distance(begin,end));
        
        // calculate position of nth-element
        // first element of bucket with nth_element inside
        int first_element_bucket_with_nth = 0;
        // partial sum to determine witch bucket contain nth_element
        int sum_bucket_size = 0;
        int bucket_wnth = 0;
        
        for(int i = 0; i < *num_buckets; i++){
            
            sum_bucket_size += count_elements_each_bucket[i];
            if(position < sum_bucket_size) {
                
                bucket_wnth = i;
                //first_element_bucket_with_nth -> define distance between begin and bucket with nth_element
                first_element_bucket_with_nth = sum_bucket_size - count_elements_each_bucket[i];
                break;
            }
        }
        
        // updating begin and end
        _random new_begin = current_begin + first_element_bucket_with_nth;
        _random new_end = current_begin + first_element_bucket_with_nth + count_elements_each_bucket[bucket_wnth];
        
        begin = new_begin;
        end = new_end;
        
        // new size
        size = int(std::distance(begin, end));
        position = int(std::distance(begin, nth));
        
        // clean heap
        delete ptr_thread;

    }
        // use sequential nth element for last swap
    std::nth_element(begin, nth, end, c);
    
    return;
}

// each thread compare all elements of the left block to the splitter. If one left element is major than splitter it will swapped with an element of right block minor than the splitter
template<class _iterator_random, class _t_comparator , typename _ValueType>
void *p_swap_chunk(void *threadarg){
    
    struct _swap_chunk_parallel<_iterator_random,_t_comparator,_ValueType > *data;
    data = (struct _swap_chunk_parallel<_iterator_random,_t_comparator ,_ValueType> *) threadarg;
    
    _ValueType s = data->single_splitter;
    
    _iterator_random s_left = data->s_left;
    _iterator_random e_left = data->e_left - 1;
    _iterator_random s_right = data->s_right ;
    _iterator_random e_right =  data->e_right - 1;
    
    // first swap sending major left elements on the right and the minor on the left
    while(s_left <= e_left && s_right <= e_right){
        if (data->c(*(s_left), s)) {
            s_left++;
        }else{
            if(data->c(s, *(e_right))){
                --e_right;
            }else{
                std::iter_swap(e_right, s_left);
                --e_right;
                ++s_left;
            }
        }
    }
    
    // When all elements about left thread are visited and there are elements on the right not visited
    // A sequential ordering is done on the remaining right block elements
    //s_right iterator points to the first elements minor than splitter on right block
    if (s_left > e_left && s_right <= e_right){
        while(s_right < e_right){
            if (data->c(*(s_right), s)) {
                s_right++;
            }else{
                if(data->c(s, *(e_right))){
                    --e_right;
                }else {
                    std::iter_swap(s_right, e_right);
                    --e_right;
                    ++s_right;
                }
            }
        }
        
        if((s_right == e_right) && (data->c((*s_right),s))){
            ++ s_right;
        }
        
        data->ret_right_not_ordered = s_right;
        data->ret_left_not_ordered = data->e_left;
        
    // the opposite of before, return the first elements in a wrong position about thread left
    }else if(s_right > e_right && s_left <= e_left ){
        while(s_left < e_left){
            if (data->c(*(s_left), s)) {
                ++s_left;
            }else{
                if(data->c(s, *(e_left))){
                    --e_left;
                }else {
                    std::iter_swap(s_left, e_left);
                    --e_left;
                    ++s_left;
                }
            }
        }
        
        if((s_left == e_left) && (data->c((*s_left),s))){
            ++ s_left;
        }
        
        data->ret_right_not_ordered = data->s_right;
        data->ret_left_not_ordered = s_left;
        
    // all elements are visited no more swap are necessary
    }else {
        data->ret_right_not_ordered = data->s_right;
        data->ret_left_not_ordered = data->e_left;
    }
    
    // return a pointer to struct with the new iterator defined by each thread
    pthread_exit(data);
}

// each thread gets a quadruple from a shared queue and swap all elements
//this quadruple is like two_blocks_swapping
template<class _iterator_random, class _t_comparator>
void *p_swap_blocks(void *threadarg){    
    
    struct _swap_blocks_parallel<_iterator_random, _t_comparator> *data;
    data = (struct _swap_blocks_parallel<_iterator_random, _t_comparator> *) threadarg;
    
    std::array<_iterator_random, 4> iter_data_to_swap;
    bool not_empty = data->pq->pop(iter_data_to_swap);
    
    while(not_empty){
        
        _iterator_random start = ( iter_data_to_swap[0]);
        _iterator_random end =  iter_data_to_swap[1];
        _iterator_random start_second_part =  iter_data_to_swap[2];
        
        for (_iterator_random a = start; a < end; a++) {
            std::iter_swap(a, start_second_part);
            start_second_part++;
        }
        
        not_empty = data->pq->pop(iter_data_to_swap);
    }
    
    pthread_exit(NULL);
}

#endif /* p_nth_element_hpp */
