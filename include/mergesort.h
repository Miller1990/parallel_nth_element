//
//  mergesort.h
//  p_nth_element_def
//
//  Created by Marco Evangelisti on 09/05/17.
//  Copyright © 2017 Marco Evangelisti. All rights reserved.
//

#ifndef mergesort_h
#define mergesort_h


#include <iostream>
#include <string>
#include <sstream>
#include <string.h>
#include <vector>

namespace KW {
    
    template<typename RI, class _Tp>
    void merge(RI left, RI end_left,
               RI right, RI end_right,
               RI out, _Tp* d) {
        
        // while there is data in both input sequences
        while (left != end_left && right != end_right) {
            // find the smaller and
            // insert it into the output sequence
            if (d(*(*left) , *(*right))) {
                *out++ = *left++;
            }
            else {
                *out++ = *right++;
            }
        }
        
        // Assert: one of the sequences has more items to copy.
        // Copy remaining input from left sequence into ouput
        while (left != end_left) {
            *out++ = *left++;
        }
        
        //copy remaining input from right sequence into output
        while (right != end_right) {
            *out++ = *right++;
        }
    }
    template<typename RI, class _Tp>
    void merge_sort(RI first, RI last, _Tp* d) {
        if (last - first > 1) {
            // split table into two new half tables
            typedef typename std::iterator_traits<RI>::value_type value_type;
            RI middle = first + (last - first) / 2;
            std::vector<value_type> left_table(first, middle);
            std::vector<value_type> right_table(middle, last);
            
            // sort the halves
            KW::merge_sort(left_table.begin(), left_table.end(),d);
            KW::merge_sort(right_table.begin(), right_table.end(),d);
            
            // merge the halves back into the original table.
            merge(left_table.begin(), left_table.end(),
                  right_table.begin(), right_table.end(),
                  first,d);
        }
    }
    
}




#endif /* mergesort_h */
