#include <pthread.h>

// This is a single lock FIFO queue, spmc
template <typename T> class  queue_spmc_single{
private:

    struct node {
        T value;
        struct node * next;
    };

    struct node *head;
    struct node *tail;
    pthread_mutex_t h_mutex;

public:
    queue_spmc_single() {
        pthread_mutex_init(&h_mutex,NULL);
        struct node * n = new struct node();
        n->next = NULL;
        head = tail = n;
    };

    bool pop( T &e) {
        struct node * n;
        pthread_mutex_lock(&h_mutex);
        n = head;
        struct node * new_head = n->next;
        if (new_head == NULL) {
            pthread_mutex_unlock(&h_mutex);
            return false;
        }
        e = new_head->value;
        head = new_head;
        pthread_mutex_unlock(&h_mutex);
        delete(n);
        return true;
    }
    int size(){
        struct node * n;
        n = head;
        int size = 0 ;
        while(n->next != NULL ){
            size ++;
            n = n->next;
        }
        return size ;
    }

    void push(const T & elem) {
        struct node * n = new struct node();
        n->value = elem;
        n->next = NULL;
        tail->next = n;
        tail = n;
    }
};
