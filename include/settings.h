// singleton class

#include<stdlib.h>

class Parameters{
    
private:
    /* Here will be the instance stored. */
    static Parameters* instance;
    
    /* Private constructor to prevent instancing. */
    Parameters();
    
    int size = 10000;
    int minimal_size_vector = 10000;
    int min_size_sequential_swap = 100;
    int num_buckets = 2;
    int max_threads = (int)sysconf(_SC_NPROCESSORS_ONLN);
    
public:
    /* Static access method. */
    static Parameters* getInstance();
    
    // setter
    void set_block_size(int size) {
        this->size = size;
    }
    
    void set_min_size_sequential_swap(int size) {
        this->min_size_sequential_swap = size;
    }
    
    void set_num_buckets(int buckets) {
        this->num_buckets = buckets;
    }
    
    void set_threads(int thr) {
        this->max_threads = thr;
    }
    
    //getter
    int get_block_size() {
        return this->size;
    }
    
    int get_min_size_sequential_swap() {
        return this->min_size_sequential_swap;
    }
    
    int get_minimal_size_vector() {
        return this->minimal_size_vector;
    }
    
    int get_num_buckets() {
        return this->num_buckets;
    }
    
    int get_threads() {
        return this->max_threads;
    }
};

/* Null, because instance will be initialized on demand. */
Parameters* Parameters::instance = 0;

Parameters* Parameters::getInstance(){
    if (instance == 0){
        instance = new Parameters();
    }
    
    return instance;
}

Parameters::Parameters(){}