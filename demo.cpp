// Simulation of p_nth_element sorting using a vector of double
// In this simulation we will sort the same vector 3 times: sequential, our, OpenMP
// it's possible compare execution times

//OpenMP
#include <parallel/algorithm>
#include <omp.h>
//Our
#include "include/p_nth_element.h" // our function to sort vector
//System
#include <iostream>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <random>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <string>

struct Nodo{
    double value; // data to be sort
}t_data;

typedef std::vector<Nodo>::iterator it;

inline bool compare(const Nodo& s1, const Nodo& s2){//fuzione comparazione, ausiliaria
    return (s1.value < s2.value);
}


int main(){
    
    int threads = 8;
    int vector_size = 10000000;
    int samples_simulation = 20;
    
    Parameters* s = Parameters::getInstance();
    s->set_threads(threads);
    std::ofstream results_seq;
    
    std::string seq = "results_double.txt";
    results_seq.open(seq);
        
    // define vectors with same elements
    std::vector<int> v;
    std::vector <Nodo> v_nth; // vector of elements for sequential nth_element
    std::vector <Nodo> v_par; // vector of elements for parallel nth_element
    std::vector <Nodo> v_opm; // vector of elements for openMP nth_element
    
    int seed = time(0);
    std::minstd_rand generator(seed);
    std::uniform_real_distribution<double> distribution(0, vector_size / 100);
    
    std::minstd_rand positioner(seed);
    std::uniform_int_distribution<int> distribution_b(1, vector_size - 1);
    
    results_seq << "Sequenziale" <<"   "<< "Nostro" <<"   "<< "openMP" << std::endl;
    
    for (int i = 0; i < samples_simulation; i++) {
        v_nth.clear();
        v_par.clear();
        v_opm.clear();
        
        int position = distribution_b(positioner);
        for (int j = 0; j < vector_size; j++) {
            Nodo nodo;
            nodo.value = distribution(generator);
            v_nth.push_back(nodo);
            v_par.push_back(nodo);
            v_opm.push_back(nodo);
        }
            
        //p_nth_element
        std::chrono::high_resolution_clock::time_point t1;
        std::chrono::high_resolution_clock::time_point t2;
        std::chrono::duration<double> durationParallel;
        t1 = std::chrono::high_resolution_clock::now();
            
        //sorting with our implementation
        p_nth_element(v_par.begin(), v_par.begin() + position, v_par.end(), compare);
        
        t2 = std::chrono::high_resolution_clock::now();
        durationParallel = std::chrono::duration_cast < std::chrono::duration < double >> (t2 - t1);
            
        //nth_element
        std::chrono::high_resolution_clock::time_point t3;
        std::chrono::high_resolution_clock::time_point t4;
        std::chrono::duration<double> durationSequential;
        t3 = std::chrono::high_resolution_clock::now();
            
        // sorting with sequential version
        std::nth_element(v_nth.begin(), v_nth.begin() + position, v_nth.end(), compare);
            
        t4 = std::chrono::high_resolution_clock::now();
        durationSequential = std::chrono::duration_cast < std::chrono::duration < double >> (t4 - t3);
            
        //gnu_parallel::nth_element
        std::chrono::high_resolution_clock::time_point t5;
        std::chrono::high_resolution_clock::time_point t6;
        std::chrono::duration<double> durationOpenMP;
        t5 = std::chrono::high_resolution_clock::now();
        
        // sorting with OpenMP
        __gnu_parallel::nth_element(v_opm.begin(), v_opm.begin() + position, v_opm.end(), compare);
        
        t6 = std::chrono::high_resolution_clock::now();
        durationOpenMP= std::chrono::duration_cast < std::chrono::duration < double >> (t6 - t5);
            
        // write results on file
        results_seq << durationSequential.count() <<"   "<< durationParallel.count() <<"   "<< durationOpenMP.count() << std::endl;

        
        // print results of sorting
        std::cout << "seq:[" << v_nth[position].value << "] - our:[" << v_par[position].value << "] - openmp:[" << v_opm[position].value << "]" << std::endl;
            
        if (v_nth[position].value != v_par[position].value || v_opm[position].value != v_par[position].value ||
            v_nth[position].value != v_opm[position].value) {
            
                std::cout << "\nI risultati sono diversi!\n";
                exit(0);
            }
        }
        
    results_seq.close();
    return 0;
} 
