CXX=g++-mp-6
OPTIMIZE_FLAGS= -O3
CXXFLAGS=-c -std=c++11 -Wall -pedantic
LDFLAGS = 
LIBS = -pthread -fopenmp

SOURCES=demo.cpp
OBJECTS= $(SOURCES:.cpp=.o)
EXECUTABLE=demo_p_nth_element

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(LIBS) $(OBJECTS) -o $@ 
.cpp.o:
	$(CXX) $(CXXFLAGS) $(OPTIMIZE_FLAGS) $(LIBS) $< -o $@ 

clean:
	rm -f *.o
	rm $(EXECUTABLE)
	rm -f *.txt



